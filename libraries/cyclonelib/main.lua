-- CycloneLib - main.lua

-- Dependencies:
---- Nothing

local SUBMOD = true
local SUBMODROOT = "cyclonelib/"
local RESTREFILE = "libraries/restre"
if not RESTRE_DIR then
	if SUBMOD then
		RESTRE_DIR = SUBMODROOT
		require(RESTRE_DIR .. RESTREFILE)()
	else
		require(RESTREFILE)()
	end
end

restre_require("contents/table")
restre_require("contents/collision/collision")
restre_require("contents/misc")
restre_require("contents/modloader")
restre_require("contents/font")
restre_require("contents/player")
restre_require("contents/list")
restre_require("contents/string")
restre_require("contents/net")

restre_require("contents/Projectile")

restre_require("contents/classes/Class")
restre_require("contents/classes/Rectangle")
restre_require("contents/classes/Vector2")