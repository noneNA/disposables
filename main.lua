-- Disposables - main.lua

-- Dependencies:
---- Nothing

local SUBMOD = false
local SUBMODROOT = "disposables/"
local RESTREFILE = "libraries/restre"
if not RESTRE_DIR then
	if SUBMOD then
		RESTRE_DIR = SUBMODROOT
		require(RESTRE_DIR .. RESTREFILE)()
	else
		require(RESTREFILE)()
	end
end

if not CycloneLib then
	restre_require("libraries/cyclonelib/main")
end

restre_require("utilities/disposable/disposable")

restre_require("contents/items/items")