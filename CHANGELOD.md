# Disposables Changelog

## Disposables 0.2.4:
* Fixed Helix syncing errors

## Disposables 0.2.3:
* Shorter item descriptions and pickup text

## Disposables 0.2.2:
* Improved Keeper's Loot network code
* Fixed Keeper's Loot item spawn positions

## Disposables 0.2.1:
* Added basic multiplayer support
* Fixed pacifist sound error

## Disposables 0.2:
* Sugar Rush
* Changes to project structure
* Made the disposable functions better
* Disposables now aren't removed but indicate if they can be activated through item sprites

## Disposables 0.1.1:
* Ancesrtral Blood now activates as full on the first activation.
* Deiwn now stuns enemies.
* Ancesrtral Blood now displays its timer on the first activation.
* Fixed inconsistent ancestral blood stacking stuff.
* Clarifications and improvements for item logs.
* Ancestral blood now has an effect for the hp increase.
* Increased the hp increase of ancestral blood.

## Disposables 0.1.0:
* Initial release.