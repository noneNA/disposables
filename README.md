# Disposables

Disposables is a mod for [Risk of Rain Modloader](https://rainfusion.ml) that adds multiple items that activate themselves on specific conditions.
The latest version can be found on the repository (https://gitlab.com/noneNA/disposables) under the `develop` branch.  
The changelog can be found in `CHANGELOG.md`  

## Includes

* Ancestral Blood: Turns excess regen into additional health. Activates after not being at full hp for a certain amount of time.  
* Deiwn: Gives you a ball and chain that hit and stun enemies touching it. Activates at the start of the teleporter event.  
* G-old Lock: Locks two nearby chests and reduces their costs. Activates when two open chests are found.  
* Helix: Gives you another hp pool that heals you if you are missing health or is drained otherwise. Activates when you go below the health threshold.  
* Keeper's Loot: Makes the next boss kill give additional items, gold and experience. Activates on killing a boss.  
* Pacifist: Makes a non-combat area that blocks all damage to actors inside. Activates on getting hit at low health.  
* Spare Parts: Gives you a drone. Activates when you have no drones.  
* Sugar Rush: Greatly increases your speed for a long duration. Activates on entering a new stage.  

## Contact

You can contact me using my mail adress `nonena@protonmail.com` or through the discord under the name `none`.  