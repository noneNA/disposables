-- Disposables - contents/items/sugarrush/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.particles = {}

resources.sprites.item = restre_spriteLoad("item.png", 2, 16, 16)
resources.sprites.disposed = resources.sprites.item

resources.sprites.rush = restre_spriteLoad("rush.png", 1, 4, 4)

resources.sprites.pixel = restre_spriteLoad("pixel.png", 1, 0, 0)

local rush = ParticleType.new("Rush")
--rush:shape("pixel")
rush:sprite(resources.sprites.pixel, false, false, false)
rush:alpha(1, 0.5)
rush:speed(0.1, 0.1, 0, 0)
rush:direction(90, 90, 0, 0)
rush:life(16, 16)
--rush:scale(1, 1)
--rush:size(1, 1, 0, 0)
rush:color(Color.fromRGB(255, 255, 0))
resources.particles.rush = rush

return resources