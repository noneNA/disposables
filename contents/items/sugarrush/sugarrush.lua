-- Disposables - contents/items/sugarrush/sugarrush.lua

-- Dependencies:
---- Disposable
---- CycloneLib.Rectangle

local resources = restre_require("resources/resources")

local NAME = "Sugar Rush"
local TIER = "use"
local COOLDOWN = 2

local PARTICLE_CHANCE = 24

local SPEED = 1
local DURATION = 24 * 60

local COLOR = ({ ["common"] = "w", ["uncommon"] = "g", ["rare"] = "r", ["use"] = "or" })[TIER]

local sugarrush = Item.new(NAME)
sugarrush.displayName = NAME
sugarrush.pickupText = "Speed boost on new stage"
sugarrush.sprite = resources.sprites.item
sugarrush.color = COLOR
sugarrush.isUseItem = true
sugarrush.useCooldown = COOLDOWN

sugarrush:setTier(TIER)
sugarrush:setLog{
	group = TIER,
	--description = string.format("Gives you a speed boost that last for %s seconds. Activates at the beginning of a stage.", DURATION / 60),
	description = string.format("Great speed boost for &y&%ss&!& &or&[New stage]&!&", DURATION / 60),
	priority = ({ ["common"] = "Standard", ["uncommon"] = "&g&Priority&!&", ["rare"] = "&r&High Priority&!&", ["use"] = "&y&Volatile&!&" })[TIER],
	destination = "Even Store\nSanita Cross\nMars",
	date = "07/07/2057",
	story = "I'm fast. Yes. I'm indeed fast. What was in that thing? Why did I eat it?... What was I doing again? Yeah, the teleporter. Where even is it? I should find the teleporter. I need to get out of here.",
}

local rush = Buff.new("Rush")
rush.sprite = resources.sprites.rush
rush:addCallback("start", function(player)
	player:set("pHmax", player:get("pHmax") + SPEED)
end)
rush:addCallback("end", function(player)
	player:set("pHmax", player:get("pHmax") - SPEED)
end)

callback.register("onDraw", function()
	for _,player in ipairs(misc.players) do
		if player:hasBuff(rush) and math.chance(PARTICLE_CHANCE) then
			local r_player = CycloneLib.Rectangle.new(player)
			resources.particles.rush:burst(
				"above",
				math.floor(r_player.x + r_player.w * math.random()),
				math.floor(r_player.y + r_player.h * math.random()),
				1
			)
		end
	end
end)

local function activateRush(player)
	if Disposable.dispose(player, sugarrush) then
		player:applyBuff(rush, DURATION)
	end
end

callback.register("onStageEntry", function()
	for _,player in ipairs(misc.players) do
		activateRush(player)
	end
end)

sugarrush:addCallback("use", function(player)
	activateRush(player)
end)

Disposable.finalize(sugarrush, resources.sprites.disposed)


--#########--
-- Exports --
--#########--

export("Disposables.resources.sugarrush", resources)