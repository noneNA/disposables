-- Disposables - contents/items/pacifist/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.sounds = {}
resources.particles = {}

resources.sprites.item = restre_spriteLoad("pacifist_item", "pacifist_item.png", 2, 16, 16)
resources.sprites.buff = restre_spriteLoad("pacifist_buff", "pacifist_buff.png", 1, 4, 4)
resources.sprites.flag = restre_spriteLoad("pacifist_flag", "pacifist_flag.png", 4, 8, 8)

local block = ParticleType.new("pacifist_block")
block:shape("smoke")
block:color(Color.WHITE)
block:alpha(0.48, 0.24)
block:size(0.12, 0.24, 0, 0)
block:direction(45, 135, 0, 1.6)
block:speed(0.2, 0.4, 0, 0)
block:gravity(0.01, 90)
block:life(16, 32)
resources.particles.block = block

resources.sounds.block = Sound.find("GiantJellyHit")

return resources