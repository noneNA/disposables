-- Disposables - contents/items/pacifist/pacifist.lua

-- Dependencies:
---- Disposable

local PACIFIST_RADIUS = 128
local PACIFIST_LIFE = 512
local PACIFIST_HEAL = 0.00032
local PACIFIST_COLOR = Color.GREEN
local PACIFIST_AREA_ALPHA = 0.04
local PACIFIST_OUTLINE_ALPHA = 0.64
local PACIFIST_COOLDOWN = 2
local PACIFIST_ALPHA_RANGE = 0.02
local PACIFIST_WAVE_MULTIPLIER = 0.04
local PACIFIER_SPRITESPEED = 0.08
local PACIFIST_BUFF_DURATION = 2 * 60
local PACIFIST_BLOCK_VOLUME = 0.8 * 2
local PACIFIST_BLOCK_VOLUME_RANDOM = 0.32
local PACIFIST_BLOCK_PITCH = 0.8
local PACIFIST_BLOCK_PITCH_RANDOM = 0.16
local PACIFIST_BLOCK_OFFSET = 4
local PACIFIST_PERCENTAGE = 1/2

local resources = restre_require("resources/resources")

local pacifist = Item.new("Pacifist")
pacifist.displayName = "Pacifist"
pacifist.pickupText = "Makes a non-combat area"
pacifist.sprite = resources.sprites.item
pacifist.color = "or"
pacifist.isUseItem = true
pacifist.useCooldown = PACIFIST_COOLDOWN

pacifist:setLog{
	group = "use",
	--description = "Radiates an area upon placement that blocks any &r&damagers&!& and &g&heals&!& players inside. Activates when you get hit on &r&low health&!&.",
	description = "Blocks &r&damagers&!& in AoE &or&[Get hit on low health]&!&",
	priority = "&y&Volatile/Military&!&",
	destination = "Canyon Gleha\nArteis Corona\nVenus",
	date = "5/8/2221",
	story = "Sometimes I look back and look at all the chaos I've caused. I just need a break sometimes. I'm just glad that I have this at least. Even for a small while.",
}

pacifist:setTier("use")

local pacifist_buff = Buff.new("pacifist_buff")
pacifist_buff.sprite = resources.sprites.buff
pacifist_buff:addCallback("step", function(instance)
	instance:set("hp", instance:get("hp") * (1 + PACIFIST_HEAL))
end)

local pacifiers = {}
local function activatePacifist(player)
	--if player.useItem == pacifist then
	if Disposable.dispose(player, pacifist) then
		table.insert(pacifiers, {
			x = player.x,
			y = CycloneLib.collision.getGround(player.x, player.y),
			r = PACIFIST_RADIUS,
			t = PACIFIST_LIFE,
		})
	end
end
pacifist:addCallback("use", activatePacifist)
registercallback("preHit", function(damager, hit)
	if hit:isValid() then
		if isa(hit, "PlayerInstance") and (hit:get("hp")/hit:get("maxhp")) < PACIFIST_PERCENTAGE then activatePacifist(hit) end
		local _r = CycloneLib.Rectangle.new(hit)
		for _,pacifier in ipairs(pacifiers) do
			if _r:intersectsCircle(pacifier.x, pacifier.y, pacifier.r) then
				damager:set("damage", 0)
				resources.particles.block:burst("above", damager.x, damager.y + PACIFIST_BLOCK_OFFSET, 4)
				resources.sounds.block:play(
					PACIFIST_BLOCK_PITCH + PACIFIST_BLOCK_PITCH_RANDOM * (math.random() - 1/2),
					PACIFIST_BLOCK_VOLUME + PACIFIST_BLOCK_VOLUME_RANDOM * (math.random() - 1/2)
				)
			end
		end
	end
end)
-- [JELLIES]
local jellyObject = Object.find("Jelly")
registercallback("onStep", function()
	for i,pacifier in ipairs(pacifiers) do
		pacifier.t = pacifier.t - 1
		if pacifier.t <= 0 then CycloneLib.table.iremove(pacifiers, i)
		else
			for _,player in ipairs(misc.players) do
				local _r = CycloneLib.Rectangle.new(player)
				if _r:intersectsCircle(pacifier.x, pacifier.y, pacifier.r) then
					player:applyBuff(pacifist_buff, PACIFIST_BUFF_DURATION)
				end
			end
			-- [JELLIES]
			for _,jellyInstance in ipairs(jellyObject:findAll()) do
				local _r = CycloneLib.Vector2.new(jellyInstance.x - pacifier.x, jellyInstance.y - pacifier.y)
				_r = _r:unit() * math.max(PACIFIST_RADIUS - _r:getLength(), 0)
				jellyInstance.x, jellyInstance.y = jellyInstance.x + _r.x, jellyInstance.y + _r.y
			end
		end
	end
end)
registercallback("onGameStart", function()
	graphics.bindDepth(2, function()
		for _,pacifier in ipairs(pacifiers) do
			graphics.color(PACIFIST_COLOR)
			graphics.alpha(PACIFIST_AREA_ALPHA + math.sin(pacifier.t * PACIFIST_WAVE_MULTIPLIER) * PACIFIST_ALPHA_RANGE)
			graphics.circle(pacifier.x, pacifier.y, pacifier.r, false)
			graphics.alpha(PACIFIST_OUTLINE_ALPHA)
			graphics.circle(pacifier.x, pacifier.y, pacifier.r, true)
			graphics.drawImage{
				image = resources.sprites.flag,
				x = pacifier.x + resources.sprites.flag.xorigin,
				y = pacifier.y - resources.sprites.flag.yorigin,
				subimage = ((pacifier.t * PACIFIER_SPRITESPEED) % resources.sprites.flag.frames) + 1,
			}
		end
	end):set("persistent", 1)
end)

Disposable.finalize(pacifist)


--#########--
-- Exports --
--#########--

export("Disposables.resources.pacifist", resources)