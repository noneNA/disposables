-- Disposables - contents/items/deiwn/deiwn.lua

-- Dependencies:
---- Disposable
---- CycloneLib.Vector2

local DEIWN_CHAIN_COUNT = 8
local DEIWN_CHAIN_INCREASE = 2
local DEIWN_CHAIN_LENGTH = 4
local DEIWN_RADIUS = 4 -- Not radius
local DEIWN_MIN_DISTANCE = 0.2
local DEIWN_TRAVEL_RATIO = 0.4
local DEIWN_DAMAGE_PERIOD = 64
local DEIWN_DAMAGE = 0.4
local DEIWN_PITCH_RANGE = 0.2
local DEIWN_VOLUME = 1
local DEIWN_PITCH = 1
local DEIWN_CLIMB = 2

local resources = restre_require("resources/resources")

local deiwn = Item.new("Deiwn")
deiwn.displayName = "Deiwn"
deiwn.pickupText = "Frees you"
deiwn.sprite = resources.sprites.item
deiwn.color = "r"

deiwn:setLog{
	group = "rare",
	--description = "Gives you a &r&ball and chain&!& that deal damage to enemies upon contact. Activates when the &r&teleporter&!& is started.",
	description = "&r&Ball and chain&!& that damages and stuns &or&[Teleporter event]&!&",
	priority = "&r&High Priority&!&",
	destination = "Ancient Pit\nClacton\nMercury",
	date = "9/3/624",
	story = "With this attached to me I feel lighter than usual for some reason. I feel like this is not a punishment. I feel freedom.",
}

deiwn:setTier("rare")

local t = 0
local function resetT() t = 0 end
registercallback("onGameStart", resetT) ; registercallback("onGameEnd", resetT)
registercallback("onStep", function() t = t + 1 end)

local player_deiwns = {}
local function resetDeiwns() player_deiwns = {} end
registercallback("onGameStart", resetDeiwns) ; registercallback("onGameEnd", resetDeiwns)
local function addDeiwn(player, xpos, ypos)
	local _x,_y = (xpos or player.x),(ypos or CycloneLib.collision.getCeiling(player.x, player.y))

	if not player_deiwns[player] then player_deiwns[player] = {} end
	player_deiwns[player][#player_deiwns[player] + 1] = {}
	
	for i=1,DEIWN_CHAIN_COUNT do
		player_deiwns[player][#player_deiwns[player]][i] = CycloneLib.Vector2.new(_x, _y)
	end
end
local function extendDeiwn(player, number)
	local _number = number or (#player_deiwns[player])
	for i=1,DEIWN_CHAIN_INCREASE do
		local _count = #player_deiwns[player][_number]
		player_deiwns[player][_number][_count + 1] = CycloneLib.Vector2.new(player.x, player.y)
	end
end

local teleporterObject = Object.find("Teleporter")
registercallback("onStep", function()
	for _,teleporterInstance in ipairs(teleporterObject:findAll()) do
		if teleporterInstance and teleporterInstance:isValid() then
			if ((teleporterInstance:get("prev_active") or 0) ~= 1) and (teleporterInstance:get("active") == 1) then
				for _,player in ipairs(misc.players) do
					if Disposable.dispose(player, deiwn) then
						if player_deiwns[player] then extendDeiwn(player)
						else addDeiwn(player) end
					end
				end
			end
			teleporterInstance:set("prev_active", teleporterInstance:get("active"))
		end
	end

	for player,deiwns in pairs(player_deiwns) do
		if player:isValid() then
			for _,deiwnInstance in ipairs(deiwns) do
				local _length = #deiwnInstance
				for i=1,_length do
					local _current = deiwnInstance[i]
					local _next = (i >= _length) and player or deiwnInstance[i + 1]
					local _d = CycloneLib.Vector2.new(_next.x - _current.x, _next.y - _current.y)
					_d = _d:unit() * math.max(_d:getLength() - DEIWN_CHAIN_LENGTH, 0)
					if _d:getLength() > DEIWN_MIN_DISTANCE then _d = _d * DEIWN_TRAVEL_RATIO end
					_current = _current + _d ; deiwnInstance[i] = _current
					
					if t % DEIWN_DAMAGE_PERIOD == 0 then
						local _damager = player:fireExplosion(_current.x, _current.y, DEIWN_RADIUS/(19*2), DEIWN_RADIUS/(4*2), DEIWN_DAMAGE, nil, nil, DAMAGER_NO_PROC + DAMAGER_NO_RECALC)
						_damager:set("deiwn", 1):set("climb", i * DEIWN_CLIMB):set("stun", 1)
					end
				end
			end
		else
			player_deiwns[player] = nil
		end
	end
end)

registercallback("onHit", function(damager)
	if ((damager:get("deiwn") or 0) > 0) and not resources.sounds.hit:isPlaying() then
		damager:set("deiwn", 0)
		resources.sounds.hit:play(DEIWN_PITCH + (math.random() - 1/2) * DEIWN_PITCH_RANGE, DEIWN_VOLUME)
	end
end)

registercallback("onGameStart", function()
	graphics.bindDepth(-7, function()
		for player,deiwns in pairs(player_deiwns) do
			for _,deiwnInstance in ipairs(deiwns) do
				local _length = #deiwnInstance
				for i=1,_length do
					graphics.drawImage{
						image = (i <= 1) and resources.sprites.ball or resources.sprites.chain,
						x = deiwnInstance[i].x,
						y = deiwnInstance[i].y + math.sin(t/8 + i),
					}
				end
			end
		end
	end):set("persistent", 1)
end)

Disposable.finalize(deiwn, resources.sprites.disposed)

--#########--
-- Exports --
--#########--

export("Disposables.resources.deiwn", resources)