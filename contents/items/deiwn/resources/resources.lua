-- Disposables - contents/items/deiwn/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}
resources.sounds = {}

resources.sprites.item = restre_spriteLoad("deiwn_item", "deiwn_item.png", 1, 16, 16)
resources.sprites.disposed = restre_spriteLoad("deiwn_disposed", "deiwn_disposed.png", 1, 16, 16)
resources.sprites.ball = restre_spriteLoad("deiwn_ball", "deiwn_ball.png", 1, 4, 4)
resources.sprites.chain = restre_spriteLoad("deiwn_chain", "deiwn_chain.png", 1, 4, 4)

resources.sounds.hit = restre_soundLoad("deiwn_hit", "deiwn_hit.ogg")

return resources