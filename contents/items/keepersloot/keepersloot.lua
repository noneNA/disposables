-- Disposables - contents/items/keepersloot/keepersloot.lua

-- Dependencies:
---- Disposable

local resources = restre_require("resources/resources")

local KEEPERSLOOT_EXPCOUNT = 32
local KEEPERSLOOT_GOLDCOUNT = 32
local KEEPERSLOOT_HSPEED = 2
local KEEPERSLOOT_VSPEED = -4.2
local KEEPERSLOOT_ITEMRANGE = 32
local KEEPERSLOOT_ITEMCOUNT = 3

local keepersloot = Item.new("Keeper's Loot")
keepersloot.displayName = "Keeper's Loot"
keepersloot.pickupText = "Next boss drops more loot"
keepersloot.sprite = resources.sprites.item
keepersloot.color = "g"

keepersloot:setLog{
	group = "uncommon",
	--description = "Increases the &y&gold&!&, &b&experience&!& and &lt&item&!& drops of one boss. Activates when a boss is killed.",
	description = "Drops &y&gold&!&, &b&exp&!&, &lt&items&!& &or&[Boss kill]&!&",
	priority = "&g&Priority&!&",
	destination = "3/7/1826",
	story = "Found this laying around. I think someone put it there intentionally. I think I'll keep it.",
}

keepersloot:setTier("uncommon")

local expObject = Object.find("EfExp")
local goldObject = Object.find("EfGold")
local commonPool = ItemPool.find("common")

local exp_packet = net.Packet.new("loot_exp", function(sender, x, y, scale)
	for i=1,KEEPERSLOOT_EXPCOUNT do
		expObject:create(x, y)
		:set("hspeed", (math.random() - 1/2) * KEEPERSLOOT_HSPEED)
		:set("vspeed", (math.random() + 1/2) * KEEPERSLOOT_VSPEED)
		:set("value", scale)
	end
end)

local gold_packet = net.Packet.new("loot_gold", function(sender, x, y, scale)
	for i=1,KEEPERSLOOT_GOLDCOUNT do
		goldObject:create(x, y)
		:set("hspeed", (math.random() - 1/2) * KEEPERSLOOT_HSPEED)
		:set("vspeed", (math.random() + 1/2) * KEEPERSLOOT_VSPEED)
		:set("value", scale)
	end
end)

registercallback("onNPCDeathProc", function(actor, player)
	--if (not net.online) or (player == net.localPlayer) then
	--if net.host then
		if actor:isBoss() and Disposable.dispose(player, keepersloot) then	
			if net.host then
				local _scale = (2 * misc.director:get("enemy_buff")) - 1
				
				for i=1,KEEPERSLOOT_EXPCOUNT do
					expObject:create(actor.x, actor.y)
					:set("hspeed", (math.random() - 1/2) * KEEPERSLOOT_HSPEED)
					:set("vspeed", (math.random() + 1/2) * KEEPERSLOOT_VSPEED)
					:set("value", _scale)
				end
				exp_packet:sendAsHost(net.ALL, nil, actor.x, actor.y, _scale)
				
				for i=1,KEEPERSLOOT_GOLDCOUNT do
					goldObject:create(actor.x, actor.y)
					:set("hspeed", (math.random() - 1/2) * KEEPERSLOOT_HSPEED)
					:set("vspeed", (math.random() + 1/2) * KEEPERSLOOT_VSPEED)
					:set("value", _scale)
				end
				gold_packet:sendAsHost(net.ALL, nil, actor.x, actor.y, _scale)
				
				for i=1,KEEPERSLOOT_ITEMCOUNT do
					local _angle = math.random() * math.pi
					local _r = KEEPERSLOOT_ITEMRANGE * CycloneLib.Vector2.new(math.cos(_angle),math.sin(_angle))
					local x, y = actor.x + _r.x, actor.y - _r.y
					local item = commonPool:roll()
					item:create(x, y)
				end
			end
		end
	--end
end)

Disposable.finalize(keepersloot, resources.sprites.disposed)


--#########--
-- Exports --
--#########--

export("Disposables.resources.keepersloot", resources)