-- Disposables - contents/items/keepersloot/resources/resources.lua

-- Dependencies:
---- Disposable

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("keepersloot_item", "keepersloot_item.png", 1, 16, 16)
resources.sprites.disposed = restre_spriteLoad("keepersloot_disposed", "keepersloot_disposed", 1, 16, 16)

return resources