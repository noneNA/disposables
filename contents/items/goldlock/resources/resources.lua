-- Disposables - contents/items/goldlock/resources/resources.lua

-- Dependencies:
---- Disposable

local resources = {}
resources.sprites = {}
resources.particles = {}

resources.sprites.item = restre_spriteLoad("goldlock_item", "goldlock_item.png", 1, 16, 16)
resources.sprites.disposed = restre_spriteLoad("goldlock_disposed", "goldlock_disposed.png", 1, 16, 16)

local lock = ParticleType.new("goldlock_lock")
lock:shape("circle")
lock:color(Color.LIGHT_GRAY)
lock:alpha(0.42, 0.64)
lock:size(4.2,4.2,-0.2,0)
lock:life(24,24)
resources.particles.lock = lock

return resources