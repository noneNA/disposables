-- Disposables - contents/items/goldlock/goldlock.lua

-- Dependencies:
---- Disposable

local GOLDLOCK_TIMER_AMOUNT = 64
local GOLDLOCK_RANGE = 65536 * 2
local GOLDLOCK_DISCOUNT = 0.25

local resources = restre_require("resources/resources")

local goldlock = Item.new("G-old Lock")
goldlock.displayName = "G-old Lock"
goldlock.pickupText = "Re-locks two chests"
goldlock.sprite = resources.sprites.item
goldlock.color = "w"

goldlock:setLog{
	group = "common",
	--description = "Locks two nearby opened &lt&chests&!& back and decreases their &y&cost&!&. Activates when there are two open &lt&chests&!& close to you.",
	description = "Re-locks two &lt&chests&!& &or&[Two open chests near]&!&",
	priority = "Standard",
	destination = "The Mines\nSufax\nVenus",
	date = "6/19/1920",
	story = "It's been collecting dust for ages now. The gold doesn't show its age though.",
}

goldlock:setTier("common")

-- Priority
local chests = {
	[3] = Object.find("Chest1"),
	[2] = Object.find("Chest2"),
	[1] = Object.find("Chest5"),
}
local function lockChest(chestInstance)
	local newChest = chestInstance:getObject():create(chestInstance.x, chestInstance.y)
	newChest:set("cost", chestInstance:get("cost") * GOLDLOCK_DISCOUNT)
	chestInstance:destroy()
	resources.particles.lock:burst("below", newChest.x, newChest.y, 1)
end
registercallback("onStep", function()
	for _,player in ipairs(misc.players) do
		if Disposable.getAvailable(player, goldlock) > 0 then
			local openChestA, openChestB = nil, nil
			for _,chestObject in ipairs(chests) do
				for _,chestMatch in ipairs(chestObject:findMatching("active", 2)) do
					if (chestMatch:get("goldlock_timer") or 0) <= GOLDLOCK_TIMER_AMOUNT then
						chestMatch:set("goldlock_timer", (chestMatch:get("goldlock_timer") or 0) + 1)
					end
					if ((player.x - chestMatch.x)^2 + (player.y - chestMatch.y)^2 <= GOLDLOCK_RANGE) and (chestMatch:get("goldlock_timer") >= GOLDLOCK_TIMER_AMOUNT) then
						if openChestA then openChestB = chestMatch ; break
						else openChestA = chestMatch end
					end
				end
				if openChestB then break end
			end
			if openChestB and Disposable.dispose(player, goldlock) then
				lockChest(openChestA)
				lockChest(openChestB)
			end
		end
	end
end)

Disposable.finalize(goldlock, resources.sprites.disposed)


--#########--
-- Exports --
--#########--

export("Disposables.resources.goldlock", resources)