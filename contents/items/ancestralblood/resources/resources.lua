-- Disposables - contents/items/ancestralblood/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("ancestralblood_item", "ancestralblood_item.png", 1, 16, 16)
resources.sprites.disposed = restre_spriteLoad("ancestralblood_disposed", "ancestralblood_disposed.png", 1, 16, 16)
resources.sprites.bar = restre_spriteLoad("ancestralblood_bar", "ancestralblood_bar.png", 1, 0, 0)

return resources