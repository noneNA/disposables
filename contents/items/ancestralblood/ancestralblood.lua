-- Disposables - contents/items/ancestralblood/ancestralblood.lua

-- Dependencies:
---- Disposable

local ANCESTRALBLOOD_CAP = 100
local ANCESTRALBLOOD_RATE = 0.2
local ANCESTRALBLOOD_INCREASE = 16 / 100
local ANCESTRALBLOOD_COLOR = Color.fromRGB(144, 0, 0)
local ANCESTRALBLOOD_TIMER_COLOR = Color.fromRGB(200,0,0)
local ANCESTRALBLOOD_TIMER = 30 * 60
local ANCESTRALBLOOD_EFFECT_TIME = 32

local resources = restre_require("resources/resources")

local ancestralblood = Item.new("Ancestral Blood")
ancestralblood.displayName = "Ancestral Blood"
ancestralblood.pickupText = "You've been ill for a long time"
ancestralblood.sprite = resources.sprites.item
ancestralblood.color = "r"

ancestralblood:setLog{
	group = "rare",
	--description = "Fills a &r&blood pack&!& that gives &g&hp&!& when low or &g&hp cap&!& when full on health.\nActivates when you stay &r&damaged&!& for a while.",
	description = "Excess &g&regen&!& fills extra &r&health bar&!& &or&[Stay damaged]&!&",
	priority = "&r&High Priority/Medical&!&",
	destination = "Medical Bay",
	date = "5/10/2052",
	story = "They told me that it wouldn't hurt. How wrong they were. Well at least I have this with me now if I ever need it. I will.",
}

ancestralblood:setTier("rare")

registercallback("onPlayerHUDDraw", function(player, x, y)
	local _x, _y = x - 36, y + 38
	if player:get("ancestralblood") then
		graphics.drawImage{
			["x"] = _x,
			["y"] = _y,
			image = resources.sprites.bar,
			scale = 0.5,
		}
		graphics.alpha(1)
		graphics.color(ANCESTRALBLOOD_COLOR)
		graphics.rectangle(_x + 2, _y, _x + 1 + math.floor(160 * math.min(1, player:get("ancestralblood")/player:get("ancestralblood_cap"))), _y + 1)
	elseif (player:get("ancestralblood_timer") or 0) > 0 then
		local _w = math.floor(160 * (1 - math.min(1, player:get("ancestralblood_timer")/ANCESTRALBLOOD_TIMER)))
		graphics.alpha(1)
		graphics.color(ANCESTRALBLOOD_TIMER_COLOR)
		graphics.rectangle(_x + 2 + 80 + _w/2, _y, _x + 2 + 80 - _w/2 - 1, _y + 1)
	end
end)

registercallback("onPlayerStep", function(player)
	if (Disposable.getAvailable(player, ancestralblood) > 0) and (player:get("hp")/player:get("maxhp") < 1) then
		if ((player:get("ancestralblood_timer") or 0) >= ANCESTRALBLOOD_TIMER) and Disposable.dispose(player, ancestralblood) then
			if not player:get("ancestralblood") then player:set("ancestralblood", ANCESTRALBLOOD_CAP) end
			player:set("ancestralblood_cap", (player:get("ancestralblood_cap") or 0) + ANCESTRALBLOOD_CAP)
			player:set("ancestralblood_timer", 0)
		else
			player:set("ancestralblood_timer", (player:get("ancestralblood_timer") or 0) + 1)
		end
	else
		player:set("ancestralblood_timer", 0)
	end
	local _blood = player:get("ancestralblood")
	if _blood then
		if _blood > player:get("ancestralblood_cap") then
			player:set("maxhp_base", player:get("maxhp_base") + ANCESTRALBLOOD_INCREASE * player:get("ancestralblood"))
			player:set("ancestralblood", 0)
			player:set("ancestral_heal", ANCESTRALBLOOD_EFFECT_TIME)
		else
			if player:get("hp") >= player:get("maxhp") then
				player:set("ancestralblood", player:get("ancestralblood") + player:get("hp_regen") * (player:get("ancestralblood_cap") / ANCESTRALBLOOD_CAP))
			else
				local _heal = math.min(ANCESTRALBLOOD_RATE, player:get("maxhp") - player:get("hp"), _blood)
				player:set("ancestralblood", _blood - _heal)
				player:set("hp", player:get("hp") + _heal)
			end
		end
	end
end)

registercallback("onPlayerDraw", function(player)
	if ((player:get("ancestral_heal") or 0) > 0) and not (player:get("maxhp") == player:get("maxhpcap")) then
		graphics.drawImage{
			image = player.sprite,
			subimage = player.subimage,
			x = player.x,
			y = player.y,
			color = ANCESTRALBLOOD_COLOR,
			alpha = math.sin(math.pi * (player:get("ancestral_heal")/ANCESTRALBLOOD_EFFECT_TIME)),
			xscale = player.xscale,
			yscale = player.yscale,
			angle = player.angle,
		}
		player:set("ancestral_heal", player:get("ancestral_heal") - 1)
	end
end)

Disposable.finalize(ancestralblood, resources.sprites.disposed)


--#########--
-- Exports --
--#########--

export("Disposables.resources.ancestralblood", resources)