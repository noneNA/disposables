-- Disposables - contents/items/items.lua

-- Dependencies:
---- Nothing

local items = {
"spareparts",
"keepersloot",
"helix",
"goldlock",
"ancestralblood",
"deiwn",
"pacifist",
"sugarrush",
}

for _,name in ipairs(items) do
	restre_require(string.format("%s/%s",name,name))
end