-- Disposables - contents/items/helix/resources/resources.lua

-- Dependencies:
---- Disposable

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("helix_item", "helix_item.png", 1, 16, 16)
resources.sprites.disposed = restre_spriteLoad("helix_disposed", "helix_disposed", 1, 16, 16)
resources.sprites.healing = restre_spriteLoad("helix_healing", "helix_healing", 1, 4, 4)

return resources