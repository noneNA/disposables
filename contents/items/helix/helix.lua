-- Disposables - contents/items/helix/helix.lua

-- Dependencies:
---- Disposable

local resources = restre_require("resources/resources")

local HELIX_THRESHOLD = 0.32
local HELIX_HEAL = 4
local HELIX_RATE = 0.004
local HELIX_DEGRADE = 0.002

local helix = Item.new("Helix")
helix.displayName = "Helix"
helix.pickupText = "Heals you on low health"
helix.sprite = resources.sprites.item
helix.color = "g"

helix:setLog{
	group = "uncommon",
	--description = "Gives you a temporary &g&health&!& pool that &g&heals&!& you or &r&depletes&!& if it can't. Activates on &r&low health&!&.",
	description = "Temporary great amount of regen &or&[Low health]&!&",
	priority = "&g&Medical&!&",
	destination = "Aldsno\nNeptune",
	date = "12/11/1723",
	story = "It's a flask containing some ancient mysterious liquid. I don't like the sounds it makes. It's quite uncomfortable.",
}

helix:setTier("uncommon")

--[[
local helix_host_packet = net.Packet.new("helix_host", function(sender, net_player, helix_healing)
	local player = net_player:resolve()
	if player then
		player:set("helix_healing", helix_healing)
	end
end)

local helix_client_packet = net.Packet.new("helix_client", function(sender, helix_healing)
	sender:set("helix_healing", helix_healing)
	helix_host_packet:sendAsHost(net.EXCLUDE, sender, net_player, helix_healing)
end)

local function sendHelix(player)
	local net_player, helix_healing = player:getNetIdentity(), player:get("helix_healing")
	helix_host_packet:sendAsHost(net.ALL, nil, net_player, helix_healing)
	helix_client_packet:sendAsClient(helix_healing, helix_healing)
end
--]]

local sync_helix = CycloneLib.net.AllPacket("helix", function(net_player, helix_healing)
	local player = net_player:resolve()
	if player then
		player:set("helix_healing", helix_healing)
	end
end)

local function sendHelix(player)
	local net_player, helix_healing = player:getNetIdentity(), player:get("helix_healing")
	sync_helix(net_player, helix_healing)
end

registercallback("onStep", function()
	local players
	if net.online then
		players = { net.localPlayer }
	else
		players = misc.players
	end
	for _,player in ipairs(players) do
		if (player:get("hp") / player:get("maxhp") <= HELIX_THRESHOLD) and Disposable.dispose(player, helix) then
			player:set("helix_healing", (player:get("helix_healing") or 0) + HELIX_HEAL * player:get("maxhp"))
			sendHelix(player)
		end
		local _hpdiff = math.abs(player:get("maxhp") - player:get("hp"))
		if (player:get("helix_healing") or 0) > 0 then
			if (_hpdiff > 0) then
				local _heal = math.min(player:get("helix_healing"), HELIX_RATE * player:get("maxhp"), _hpdiff)
				player:set("helix_healing", player:get("helix_healing") - _heal)
				player:set("hp", player:get("hp") + _heal)
			else
				player:set("helix_healing", math.max(0, player:get("helix_healing") - HELIX_DEGRADE * player:get("maxhp")))
			end
		else
			sendHelix(player)
		end
	end
end)
--[[
registercallback("onPlayerDraw", function(player)
	if (player:get("helix_healing") or 0) > 0 then
		graphics.drawImage{
			image = resources.sprites.healing,
			x = player.x,
			y = player.y + 16,
		}
	end
end)
--]]
local helix_indicator = Buff.new("Helix Indicator")
helix_indicator.sprite = resources.sprites.healing
callback.register("onPlayerStep", function(player)
	if (player:get("helix_healing") or 0) > 0 then
		player:applyBuff(helix_indicator, 16)
	end
end)

Disposable.finalize(helix, resources.sprites.disposed)


--#########--
-- Exports --
--#########--

export("Disposables.resources.helix", resources)