-- Disposables - contents/items/spareparts/spareparts.lua

-- Dependencies:
---- Disposable

local SPAREPARTS_HP = 64

local resources = restre_require("resources/resources")

local spareparts = Item.new("Spare Parts")
spareparts.displayName = "Spare Parts"
--spareparts.pickupText = "Gives you a drone if you have no other"
spareparts.pickupText = "Drone to one without"
spareparts.sprite = resources.sprites.item
spareparts.color = "w"

spareparts:setLog {
	group = "common",
	--description = "Gives you a &lt&drone&!&. Activates when you have no &lt&drones&!&.",
	description = "Gives a &lt&drone&!& &or&[Have no drones]&!&",
	priority = "Standard/Military",
	destination = "Cosmic Junkyard",
	date = "--/--/----",
	story = "Don't even bother. It's broken.",
}

spareparts:setTier("common")

local o_assembled_drone = Object.find("Drone2")
local drone_objects = {
	Object.find("Drone1"),
	Object.find("Drone2"),
	Object.find("Drone3"),
	Object.find("Drone4"),
	Object.find("Drone5"),
	Object.find("Drone6"),
	Object.find("Drone7"),
	Object.find("DroneDisp"),
}

registercallback("onStep", function()
	local drone_counts = {}
	for k,v in ipairs(misc.players) do
		drone_counts[v.id] = 0
	end
	for _,o_drone in ipairs(drone_objects) do
		for _,i_drone in ipairs(o_drone:findAll()) do
			local master = i_drone:get("master")
			if drone_counts[master] then
				drone_counts[master] = drone_counts[master] + 1
			end
		end
	end
	for k,v in pairs(drone_counts) do
		local player = Object.findInstance(k)
		if player and player:isValid() then
			if (v == 0) and Disposable.dispose(player, spareparts) then
				local droneInstance = o_assembled_drone:create(player.x, player.y)
				:set("master", player.id)
				:set("maxhp",  SPAREPARTS_HP)
				:set("lastHp", SPAREPARTS_HP)
				:set("hp",     SPAREPARTS_HP)
			end
		end
	end
end)

Disposable.finalize(spareparts, resources.sprites.disposed)


--#########--
-- Exports --
--#########--

export("Disposables.resources.spareparts", resources)