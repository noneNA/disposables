-- Disposables - contents/items/spareparts/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sprites = {}

resources.sprites.item = restre_spriteLoad("spareparts_item", "spareparts_item", 1, 16, 16)
resources.sprites.disposed = restre_spriteLoad("spareparts_disposed", "spareparts_disposed", 1, 16, 16)

return resources