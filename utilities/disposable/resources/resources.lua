-- Disposables - utilities/disposable/resources/resources.lua

-- Dependencies:
---- Nothing

local resources = {}
resources.sounds = {}

resources.sounds.dispose_effect = Sound.find("Pickup")

return resources