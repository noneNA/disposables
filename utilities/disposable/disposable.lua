-- Disposables - utilities/disposable/disposable.lua

-- Dependencies:
---- [Optional] IRL

local Y_OFFSET = 16
local PARTICLE_LIFE = 120
local PARTICLE_ALPHA = 1
local PARTICLE_GRAVITY = 0.08
local PARTICLE_GRAVITY_DIRECTION = 90

local resources = restre_require("resources/resources")

local Disposable = {}

local disposable_items = {}
local disposable_particles = {}
local disposable_sprites = {}
local disposed_sprites = {}

Disposable.getTotal = function(player, item)
	if item.isUseItem then
		return (player.useItem == item) and 1 or 0
	end
	return player:countItem(item)
end

Disposable.getDisposed = function(player, item)
	local disposed = player:get(item:getName() .. "_disposed")
	if not disposed then return 0 end
	return disposed
end

Disposable.setDisposed = function(player, item, count)
	local set
	if count >= 0 then set = count end
	player:set(item:getName() .. "_disposed", set)
	if Disposable.getAvailable(player, item) > 0 then
		player:setItemSprite(item, disposable_sprites[item])
	else
		player:setItemSprite(item, disposed_sprites[item])
	end
end

Disposable.getAvailable = function(player, item)
	local disposed = Disposable.getDisposed(player, item)
	return Disposable.getTotal(player, item) - (disposed or 0)
end

Disposable.dispose = function(player, item)
	local count = Disposable.getAvailable(player, item)
	local available = count > 0
	if not available then
		return available
	end
	if not item.isUseItem then
		Disposable.setDisposed(player, item, Disposable.getDisposed(player, item) + 1)
		--[[
		if count == 1 and disposed_sprites[item] then
			player:setItemSprite(item, disposed_sprites[item])
		end
		--]]
	else
		local restore_use_item = player:get("restore_use_item")
		player.useItem = restore_use_item and Item.find(restore_use_item)
	end
	local x, y = player.x, player.y - Y_OFFSET
	local particle = disposable_particles[item]
	if not particle then return nil end
	particle:burst("middle", x, y, 1)
	resources.sounds.dispose_effect:play()
	return available
end

Disposable.finalize = function(item, disposed_sprite)
	item:addCallback("pickup", function(player)
		if player:get(item:getName() .. "_disposed") == nil then
			Disposable.setDisposed(player, item, 0)
		end
		player:setItemSprite(item, disposable_sprites[item])
	end)
	local particle = disposable_particles[item]
	if not particle then
		particle = ParticleType.new(item:getName().."_particle")
		particle:life(PARTICLE_LIFE, 120)
		particle:alpha(PARTICLE_ALPHA, 0)
		particle:gravity(PARTICLE_GRAVITY, PARTICLE_GRAVITY_DIRECTION)
		particle:sprite(item.sprite, false, false, false)
		disposable_particles[item] = particle
	end
	disposable_sprites[item] = item.sprite
	disposed_sprites[item] = disposed_sprite
	disposable_items[item] = true
	
	if itemremover then
		itemremover.setRemoval(item, function(player, desiredcount)
			local disposed = Disposable.getDisposed(player, item)
			local total = Disposable.getTotal(player, item)
			Disposable.setDisposed(player, item, math.min(total, disposed))
		end)
	end
end

callback.register("onStep", function()
	for _,player in ipairs(misc.players) do
		local use = player.useItem
		if disposable_items[use] then
			player:set("restore_use_item", player:get("last_use_item"))
		else
			player:set("last_use_item", use and use:getName())
		end
	end
end)


--#########--
-- Exports --
--#########--

_G.Disposable = Disposable
export("Disposables.Disposable", Disposable)
export("Disposables.resources.Disposable", resources)
return Disposable